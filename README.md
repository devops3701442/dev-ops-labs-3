# DevOps-labs: 3.Airflow + Spark + CI/CD

## Описание сервиса
Сервис включает в себя настроенный файл docker-compose для развертывания Apache Airflow и ориентированный ациклический граф (DAG), состоящий из двух задач (tasks).    \
Происходит подключение Airflow к Apache Spark и через SparkSession выполняется DAG с двумя задачами:    
1) загрузка датасета ирисов;    
2) многоклассовая классификация с использованием модели логистической регрессии.        
    
В логах выводится пять строк датасета и точность (Accuracy) модели классификации.        

![data_head](./images/data_head.png)

![Accuracy](./images/Accuracy.png)

Примечания:
* В docker-compose файле вырезаны сервисы redis, airflow-worker, airflow-triggerer, airflow-cli, flower    
* USER_USERNAME и USER_PASSWORD оставлены дефолтными (airflow:airflow).При желании можно заполнить значения своими переменными.

## Локальный деплой сервиса

`git clone https://gitlab.com/devops3701442/devops-labs_3.git`

`cd devops-labs_3`

`echo -e "AIRFLOW_UID=$(id -u)" > .env`

`docker-compose up -d`

`http://localhost:8080/`

Настройка Admin Connections (Admin → Connections → + New):
* Connection Id: `spark_local`    
* Connection Type: `Spark`    
* Host: `spark://spark-master`    
* Port: `7077`    

![Add_Connection](./images/Add_Connection.png)

Запуск DAG:
![DAGs](./images/DAGs.png)

В админке Spark (`http://localhost:4040/`) отражается worker и завершенные Applications:
![Spark](./images/Spark.png)


## Пайплайн CI/CD в Gitlab:
Файл .gitlab-ci.yml содержит конфиг пайплайна.    \
Пайплайн состоит из трех этапов: `test` (тестирование), `build` (сборка) и `deploy` (развертывание). Шаг тестирования выполняется всегда во всех ветках.    \
Задачи (jobs):
- `test-job`: в этой задаче проверяется наличие директорий "dags" и "spark". Если хотя бы одна из них отсутствует, выводится сообщение об ошибке, и выполнение завершается с кодом 1. Если обе директории "dags" и "spark" существуют, выводится сообщение об успешном прохождении теста.
- `build-job`: в этой задаче проверяется, что задача выполняется только для веток с именами, начинающимися с "main", "master" или "develop" и исключаются ветки, начинающиеся с "feature/". Выполняется сборка образа Docker.
- `deploy-job`: в этой задаче проверяется, что задача выполняется только для веток с именами, начинающимися с "main", "master" или "develop". Используется образ Docker tmaier/docker-compose:latest для развертывания приложения с использованием Docker Compose.
- `clear-job` в этой задаче очищается среда. Задача выполнится по умолчанию только вручную, так как у нее установлен параметр when: manual.

## Локальный деплой раннера
1) Форк репозитория
2) Локально создается файл `docker-compose.yaml`:

```yaml
version: '3.7'
services:
  gitlab-runner:
    image: gitlab/gitlab-runner:alpine
    container_name: gitlab-runner
    restart: always
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
```
3) `docker-compose up -d`
4) В Gitlab репозитория: Settings --> CI/CD --> Runners --> Project runners --> рядом с кнопкой New project runner открыть опции и скопировать Registration token
5) `docker exec -it gitlab-runner gitlab-runner register --url "https://gitlab.com/" --registration-token <Registration token>`    \
Enter the GitLab instance URL (for example, https://gitlab.com/):    \
[https://gitlab.com/]:     
Enter the registration token:    \
[Registration token]:     
Enter a description for the runner: `example_runner`    \
Enter tags for the runner (comma-separated): `docker_labs`    \
Enter optional maintenance note for the runner:    \
Enter an executor: virtualbox, docker+machine, custom, docker, docker-windows, shell, ssh, parallels, docker-autoscaler, instance, kubernetes: `docker`    \
Enter the default Docker image (for example, ruby:2.7): `docker:stable`
6) `docker exec -it gitlab-runner bash`
7) `vi /etc/gitlab-runner/config.toml`
8) В конце файла дописать строчку: `volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]`
9) `docker restart gitlab-runner`
10) Убедиться, что раннер “подцепился” в разделе Assigned project runners
11) Убедиться, что в Build --> Pipelines все четыре Jobs успешно исполнились.
