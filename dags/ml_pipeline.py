from datetime import datetime, timedelta
from airflow.operators.python_operator import PythonOperator
from airflow.providers.apache.spark.operators.spark_submit import SparkSubmitOperator
from airflow import DAG

current_datetime = datetime.now()

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1)
}

dag = DAG(
    'ml_model',
    default_args=default_args,
    schedule_interval=timedelta(days=1),
    start_date=current_datetime,
    catchup=False
    )

spark_job_download_data = SparkSubmitOperator(
    task_id='spark_job_download_data',
    application=f'/opt/airflow/spark/load_data.py',
    name='load_data',
    conn_id='spark_local',
    dag=dag)

spark_job_reg_model = SparkSubmitOperator(
    task_id='spark_job_reg_model',
    application=f'/opt/airflow/spark/log_reg_model.py',
    name='log_reg_model',
    conn_id='spark_local',
    dag=dag)

spark_job_download_data >> spark_job_reg_model






