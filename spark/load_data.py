import pandas as pd
import pyspark
from sklearn import datasets

from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession

# spark_job
conf = SparkConf().setAppName("IrisData").setMaster("spark://spark-master:7077")
sc = SparkContext(conf=conf)

# Start the SparkSession
spark = SparkSession.builder \
    .config(conf=conf) \
    .getOrCreate()

# Dataset
iris = datasets.load_iris()

df = pd.DataFrame({
    'sepal-length':iris.data[:,0],
    'sepal-width':iris.data[:,1],
    'petal-length':iris.data[:,2],
    'petal-width':iris.data[:,3],
    'class':iris.target
    })
    
print(df.head(5))

# Save the DataFrame
df.to_csv("iris_data.csv", header=True, index=False)


    
