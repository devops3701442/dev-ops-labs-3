import pandas as pd
import pyspark

from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
from pyspark.ml.feature import StandardScaler
#from pyspark.ml.feature import VectorAssembler
#from pyspark.ml.classification import LogisticRegression

# spark_job
conf = SparkConf().setAppName("Log_Reg_Model").setMaster("spark://spark-master:7077")
sc = SparkContext(conf=conf)

# Start the SparkSession
spark = SparkSession.builder \
    .config(conf=conf) \
    .getOrCreate()

# Uploading data to another Spark task on the same Spark cluster.
df = pd.read_csv("iris_data.csv")

# Create a Spark DataFrame from a Pandas DataFrame
data = spark.createDataFrame(df)

data.show(5)

# vectorize all numerical columns into a single feature column
feature_cols = data.columns[:-1]
assembler = pyspark.ml.feature.VectorAssembler(inputCols=feature_cols, outputCol='features')
data = assembler.transform(data)

# Convert text labels into indices
data = data.select(['features', 'class'])
label_indexer = pyspark.ml.feature.StringIndexer(inputCol='class', outputCol='label').fit(data)
data = label_indexer.transform(data)

# Select the features and label column
data = data.select(['features', 'label'])
    
# Scaling
scaler = StandardScaler(inputCol = 'features', outputCol = 'scaledFeatures')
scaler_model = scaler.fit(data)
final_data = scaler_model.transform(data).select('label', 'scaledFeatures')
# Drop the 'features' column and rename 'scaledFeatures' to 'features'
final_data = final_data.select('scaledFeatures', 'label').withColumnRenamed('scaledFeatures', 'features')

# Use Logistic Regression to train on the training set
train, test = final_data.randomSplit([0.8, 0.2])
log_reg = pyspark.ml.classification.LogisticRegression()
model = log_reg.fit(train)

# Predict on the test set
prediction = model.transform(test)

# Evaluate the accuracy of the model using the test set
evaluator = pyspark.ml.evaluation.MulticlassClassificationEvaluator(metricName='accuracy')
accuracy = evaluator.evaluate(prediction)

print('=' * 50)
print("Accuracy:", round(accuracy, 2))
print('=' * 50)

# Stop the SparkSession
spark.stop()
